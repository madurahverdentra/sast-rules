// License: LGPL-3.0 License (c) find-sec-bugs
package random

import scala.util.Random

object Some {
    def generateSecretToken() {
        // ruleid: scala_random_rule-PseudoRandom
        print(Random.nextBoolean())
        // ruleid: scala_random_rule-PseudoRandom
        print(Random.nextDouble())
        // ruleid: scala_random_rule-PseudoRandom
        print(Random.nextFloat())
        // ruleid: scala_random_rule-PseudoRandom
        print(Random.nextGaussian())
        // ruleid: scala_random_rule-PseudoRandom
        print(Random.nextInt())
        // ruleid: scala_random_rule-PseudoRandom
        print(Random.nextLong())
        // ruleid: scala_random_rule-PseudoRandom
        print(Random.nextPrintableChar())
        // ruleid: scala_random_rule-PseudoRandom
        print(Random.nextString(10))
    }
}
