#!/usr/bin/env bash

# Fetches ~SAST::Ruleset issues with missing workflow label and assigns
# the ~workflow::ready for development label to them

set -e

# gitlab-org/gitlab
projectId=278964

# Issues must have ~SAST::Ruleset label assigned.
filterLabels="SAST::Ruleset"

# Issues must not already have a ~workflow::* label.
filterLabelsNot="workflow::*"

# Performs an HTTP request to the URL provided as the first argument with
# optional method as second parameter (default GET).
#
# Access token is automatically added for requests to https://gitlab.com/ URLs.
function doRequest() {
  curlArgs=(
    "--request" "${2:-GET}" # request method from second argument or GET.
    "--location" # follow redirects.
    "--retry" "3" # retry failing requests up to 3 times.
    "--retry-delay" "10" # wait 10 seconds between each retry.
    "--silent" # only print response body.
    "--show-error" # show error if failing.
    "--fail" # fail fast on server error.
    "-g" # don't validate URLs (curl doesn't like the `&not[labels]=` query for some reason).
  )

  if [[ "$1" == "https://gitlab.com/"* ]]; then
    curlArgs+=("-H" "PRIVATE-TOKEN: $GITLAB_BOT_TOKEN")
  fi

  response="$(curl "${curlArgs[@]}" "$1" || true)"

  echo "$response"
}

data="$(doRequest "https://gitlab.com/api/v4/projects/$projectId/issues/?state=opened&labels=$filterLabels&not[labels]=$filterLabelsNot&per_page=100")"

if [ "$data" = "" ]; then
    echo "No data returned from API"
    exit 1
fi

issues="$(echo "$data" | jq -c '.[]')"

echo "Fetched $(echo "$issues" | wc -l) issues without workflow labels"

while IFS= read -r issue; do
  iid="$(echo "$issue" | jq -r '.iid')"

  doRequest "https://gitlab.com/api/v4/projects/$projectId/issues/$iid?add_labels=workflow::ready%20for%20development" "PUT" >> /dev/null

  echo "[#$iid]: assigned workflow label to issue"
done <<< "$issues"
