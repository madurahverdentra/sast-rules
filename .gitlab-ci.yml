stages:
  - maintenance
  - prepare
  - verify
  - tag
  - test
  - bap
  - release

include:
  - template: Jobs/SAST.gitlab-ci.yml
  # trigger this pipeline only for new branches or commits to the respective branch in order to  prevent duplicate pipelines
  - template: 'Workflows/Branch-Pipelines.gitlab-ci.yml'
  - component: gitlab.com/gitlab-org/components/danger-review/danger-review@~latest

danger-review:
  stage: prepare
  rules:
    # the following line is necessary because the danger-review component expects
    # a merge_request_event as CI_PIPELINE_SOURCE
    # see https://gitlab.com/gitlab-org/components/danger-review/-/blob/main/templates/danger-review/template.yml?ref_type=heads#L18
    - if: $CI_PIPELINE_SOURCE == "push"

# make sure the rules adhere to our quality guidelines
rulecheck-quality:
  stage: prepare
  image: ruby:3-alpine
  before_script:
    - apk add --no-cache yaml-dev alpine-sdk bash python3 py3-pip curl go
    - gem install psych yaml fileutils json-schema word_wrap
  script:
    - go install github.com/neilpa/yajsv@v1.4.1
    - PATH="/root/go/bin:$PATH"
    - pip3 install --break-system-packages yamllint codespell
    - yamllint .
    - ./ci/schema.rb .
    - ./ci/rule_schema.sh
    - ./ci/codespell.sh
    - ./ci/license_header.sh
  rules:
    - changes:
        - "{c,csharp,go,java,javascript,python,rules,scala}/**/*"
    - when: never

rulecheck-tests:
  stage: verify
  image: registry.gitlab.com/security-products/semgrep:latest
  script:
    - semgrep --metrics=off --test ./
  rules:
    - changes:
        - "{c,csharp,go,java,javascript,python,rules,scala}/**/*"
    - when: never

mapping-check:
  stage: verify
  image: ruby:3-alpine
  before_script:
    - apk add --no-cache yaml-dev alpine-sdk
    - gem install yaml json json-schema
  script:
    - ./ci/mappings.rb
    - ./ci/mappings-schema.rb
    - ./ci/testcase_presence_check.rb
    - git diff --exit-code
  rules:
    - changes:
        - "{c,csharp,go,java,javascript,mappings,python,rules,scala}/**/*"
    - when: never

java-build-check:
  stage: verify
  image: openjdk:17-alpine
  before_script:
    - apk add --no-cache bash maven
  script:
    ./ci/scaffold_java_build.sh
  rules:
    - changes:
        - java/**/*.java
    - when: never

scala-build-check:
  stage: verify
  image: sbtscala/scala-sbt:eclipse-temurin-18.0.1_1.7.1_2.13.8
  before_script:
    - apt update
    - apt install -y maven
  script:
    ./ci/scaffold_scala_build.sh
  rules:
    - changes:
        - scala/**/*.scala
    - when: never

build-and-verify:
  stage: verify
  image: ruby:3-alpine
  before_script:
    - apk update && apk add bash jq nodejs npm git python3 py3-pip zip curl
    - npm install -g changelog-parser
    - gem install word_wrap
    - export VERSION=$(curl "https://gitlab.com/gitlab-org/security-products/analyzers/semgrep/-/raw/main/Dockerfile" | sed -n 's/ARG SCANNER_VERSION=\([^ ]*\)/\1/p')
    - echo "Installing semgrep version $VERSION"
    - pip3 install --break-system-packages semgrep==$VERSION
  script:
    - export CHANGELOG_VERSION=$(changelog-parser | jq -r '.versions[0].title')
    - ./ci/run_deploy.rb $CHANGELOG_VERSION
    - ci/semgrep_validity_check.sh
    - ci/unique_ids.rb
    - echo "EXTRA_DESCRIPTION=sast-rules release" >> variables.env
    - echo "TAG=${CHANGELOG_VERSION}" >> variables.env
    - echo "BRANCH_RELEASE_NAME=${CI_COMMIT_BRANCH}-${CI_COMMIT_SHA}" >> variables.env
    - zip -r sast-rules.zip ./dist
  artifacts:
    reports:
      dotenv: variables.env
    expose_as: "sast-rules"
    paths: ["sast-rules.zip"]
    when: on_success
  rules:
    - changes:
        - "{c,csharp,go,java,javascript,mappings,python,rules,scala}/**/*"
        - "CHANGELOG.md"
    - when: never

deploy-test:
  stage: test
  image: ruby:3.2.2-alpine
  script:
    - gem install rspec
    - rspec

branch_release_job:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: never
    - when: manual
      allow_failure: true
  needs:
    - job: build-and-verify
      artifacts: true
      optional: true
  script:
    - apk --no-cache add curl
    - echo "Running release_job for $TAG"
    - echo "Publishing ${RELEASE_SHA}"
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file sast-rules.zip "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/sast-rules/${BRANCH_RELEASE_NAME}/sast-rules.zip"

.bap-require-rules:
  rules:
    - if: $RUN_ASSIGN_PRIORITY_LABELS
      when: never
    - if: $RUN_ASSIGN_WORKFLOW_LABELS
      when: never
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      when: manual

bap-analysis-generate-work:
  stage: bap
  image: docker:20.10
  extends: .bap-require-rules
  services:
    - docker:20.10-dind
  variables:
    TARGET_TAG: $CI_COMMIT_SHA$IMAGE_TAG_SUFFIX
    SEMGREP_PROJECT_ID: "24075172"
  before_script:
    - apk add --no-cache jq curl sed git zip bash
  script:
    - ./ci/bap-docker-build.sh
  artifacts:
    paths:
      - work.json
    reports:
      dotenv: build.env

bap-analysis-trigger-analysis:
  stage: bap
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes:
        - "{c,csharp,go,java,javascript,mappings,python,rules,scala}/**/*.yml"
      when: always
    - when: never
  needs:
    - job: bap-analysis-generate-work
      artifacts: true
  variables:
    UPSTREAM_PROJECT_ID: $CI_PROJECT_ID
    UPSTREAM_JOB_ID: $UPSTREAM_JOB_ID
    BAP_COMPARE: "true"
  trigger:
    project: gitlab-org/secure/tools/bap
    strategy: depend

bap-analysis-verify-analysis:
  stage: bap
  image: alpine:3.18.4
  rules:
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      changes:
        - "{c,csharp,go,java,javascript,mappings,python,rules,scala}/**/*.yml"
      when: always
    - when: never
  needs:
    - bap-analysis-trigger-analysis
  variables:
    DOWNSTREAM_PROJECT_ID: "50773167" # BAP project
  before_script:
    - apk add --no-cache jq git curl make musl-dev go zip bash
  script:
    - ./qa/bap/verify.sh
  artifacts:
    when: always
    paths:
      - ./scan-reports/gl-bap-analysis-report.json
      - ./scan-reports/gl-bap-analysis-report.html
  allow_failure: false

release_job:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: build-and-verify
      artifacts: true
      optional: true
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
  script:
    - apk --no-cache add curl
    - echo "Running release_job for $TAG"
    - echo "Publishing ${TAG}"
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file sast-rules.zip "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/sast-rules/${TAG}/sast-rules.zip"
    - |
      curl --request POST --header "JOB-TOKEN: ${CI_JOB_TOKEN}" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/tags?tag_name=${TAG}&ref=main"
  release:
    name: 'Release $TAG'
    description: '$EXTRA_DESCRIPTION'
    tag_name: '$TAG'
    ref: '$CI_COMMIT_SHA'
    assets:
      links:
        - name: 'sast-rules'
          url: '${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/sast-rules/${TAG}/sast-rules'

assign-priority-labels:
  stage: maintenance
  rules:
    - if: $RUN_ASSIGN_PRIORITY_LABELS
  image: alpine:3.19
  before_script:
    - apk add --no-cache bash curl jq
  script:
    - ./ci/issues_severity_assign.sh

assign-workflow-labels:
  stage: maintenance
  rules:
    - if: $RUN_ASSIGN_WORKFLOW_LABELS
  image: alpine:3.19
  before_script:
    - apk add --no-cache bash curl jq
  script:
    - ./ci/issues_workflow_assign.sh
