import javax.net.ssl.SSLServerSocketFactory;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.ServerSocket;

public class UnencryptedSocket {

    private int port = 8080;
    private int backlog = 80;

    private InetAddress localAddress = InetAddress.getByName("192.168.1.100");

    public UnencryptedSocket(int port, int backlog, InetAddress localAddress) {
        this.port = port;
        this.backlog = backlog;
        this.localAddress = localAddress;
    }

    public void serverSocketBacklog() throws IOException {
        // ruleid: java_crypto_rule-UnencryptedSocket
        ServerSocket serverSocket = new ServerSocket(this.port, this.backlog);
        serverSocket.close();
        // Additional code for server socket operations can be added here
    }

    public void serverSocketLocalAddress() throws IOException {
        // ruleid: java_crypto_rule-UnencryptedSocket
        ServerSocket serverSocket = new ServerSocket(this.port, this.backlog, this.localAddress); // Port: 8080, Backlog: 50, Local Address: 192.168.1.100
        serverSocket.close();
    }

    public void plainServerSocket() throws IOException {
        // ruleid: java_crypto_rule-UnencryptedSocket
        ServerSocket serverSocket = new ServerSocket(this.port);
        serverSocket.close();
    }

    public void plainSocket() throws IOException {
        // ruleid: java_crypto_rule-UnencryptedSocket
        Socket socket = new Socket("localhost", this.port);
        socket.close();
    }

    public void remoteSocket() throws IOException {
        // ruleid: java_crypto_rule-UnencryptedSocket
        Socket socket = new Socket("example.com", this.port);
        socket.close();
    }

    public void socketWithConnectionTimeout () throws  IOException {
        // ruleid: java_crypto_rule-UnencryptedSocket
        Socket socket = new Socket();
        socket.connect(new InetSocketAddress("example.com", this.port), 5000); // Connect with a timeout of 5 seconds
        socket.close();
    }

    public void socketConnectionWithProxy () throws  IOException {

        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("proxy.example.com", this.port));
        // ruleid: java_crypto_rule-UnencryptedSocket
        Socket socket = new Socket(proxy);
        socket.connect(new InetSocketAddress("example.com", 80));

        socket.close();
    }

    static void sslServerSocket() throws IOException {
        // ok: java_crypto_rule-UnencryptedSocket
        ServerSocket ssoc = SSLServerSocketFactory.getDefault().createServerSocket(1234);
        ssoc.close();
    }

    static void sslSocket() throws IOException {
        // ok: java_crypto_rule-UnencryptedSocket
        Socket soc = SSLSocketFactory.getDefault().createSocket("www.google.com", 443);
        doGetRequest(soc);
    }
}
