# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "java_password_rule-HardcodeKey"
  languages:
  - "java"
  pattern-either:
  - patterns:
    - pattern-not-inside: |
        $FUNC(...,$TYPE $KEY_BYTES, ...) {
        ...
        }
    - pattern-not-inside: |
        $SC = (KeyGenerator $KG).generateKey();
        ...
        byte[] $KEY_BYTES = $SC.getEncoded();
        ...
    - pattern-either:
      - pattern: "new DESKeySpec((byte[] $KEY_BYTES), ...);"
      - pattern: "new DESKeySpec($KEY_BYTES.getBytes(), ...);"
      - pattern: "new DESedeKeySpec((byte[] $KEY_BYTES), ...);"
      - pattern: "new DESedeKeySpec($KEY_BYTES.getBytes(), ...);"
      - pattern: "new KerberosKey(..., (byte[] $KEY_BYTES), ...);"
      - pattern: "new KerberosKey(..., $KEY_BYTES.getBytes(), ...);"
      - pattern: "new SecretKeySpec((byte[] $KEY_BYTES), ...);"
      - pattern: "new SecretKeySpec($KEY_BYTES.getBytes(), ...);"
      - pattern: "new X509EncodedKeySpec((byte[] $KEY_BYTES));"
      - pattern: "new X509EncodedKeySpec($KEY_BYTES.getBytes());"
      - pattern: "new PKCS8EncodedKeySpec((byte[] $KEY_BYTES));"
      - pattern: "new PKCS8EncodedKeySpec($KEY_BYTES.getBytes());"
      - pattern: "new KeyRep(...,(byte[] $KEY_BYTES));"
      - pattern: "new KeyRep(...,$KEY_BYTES.getBytes());"
      - pattern: "new KerberosTicket($ASN, $CLIENT, $SERVER, (byte[] $KEY_BYTES), ...);"
      - pattern: "new KerberosTicket($ASN, $CLIENT, $SERVER, $KEY_BYTES.getBytes(), ...);"
      - pattern: "new HmacUtils(..., (byte[] $KEY_BYTES));"
      - pattern: "new HmacUtils(..., $KEY_BYTES.getBytes());"
      - pattern: "new HmacUtils(..., (String $KEY_BYTES));"
      - pattern: "new KeyParameter((byte[] $KEY_BYTES), ...);"
      - pattern: "new KeyParameter($KEY_BYTES.getBytes(), ...);"
      - patterns:
        - pattern-either:
          - pattern: "Algorithm.$HMAC((byte[] $KEY_BYTES));"
          - pattern: "Algorithm.$HMAC($KEY_BYTES.getBytes());"
          - pattern: "Algorithm.$HMAC((String $KEY_BYTES));"
        - metavariable-regex:
            metavariable: "$HMAC"
            regex: "(HMAC384|HMAC256|HMAC512)"
    - metavariable-pattern:
        metavariable: "$KEY_BYTES"
        patterns:
        - pattern-not-regex: "(null)"
  - patterns:
    - pattern-either:
      - patterns:
        - pattern-either:
          - pattern-inside: |
              BigInteger $PRIVATE_KEY = new BigInteger(...);
              ...
          - pattern-inside: |
              class $CLS{
              ...
              BigInteger $PRIVATE_KEY = new BigInteger(...);
              ...
              }
        - pattern: |
            new $METHOD($PRIVATE_KEY, ...);
      - patterns:
        - pattern-either:  
          - pattern-inside: |
              class $CLS{
                ...
                $TYPE $PRIVATE_KEY = ...
                ...
              }
          - pattern-inside: |
              $T $FUNC(...) {
                ...
                $TYPE $PRIVATE_KEY = ...;
                ...
              }    
        - pattern: |
            new $METHOD(new BigInteger($PRIVATE_KEY), ...);              
    - metavariable-pattern:
        metavariable: "$PRIVATE_KEY"
        patterns:
        - pattern-not-regex: "(null)"
    - metavariable-regex:
        metavariable: "$METHOD"
        regex: "(DSAPrivateKeySpec|DHPrivateKeySpec|ECPrivateKeySpec|RSAPrivateKeySpec|RSAMultiPrimePrivateCrtKeySpec|RSAPrivateCrtKeySpec)"
  message: |
    A potential cryptographic key was identified in a hard-coded string.
    Cryptographic keys should not be stored directly in code
    but loaded from secure locations such as a Key Management System (KMS).

    The purpose of using a Key Management System is to ensure that access can 
    be audited and that the keys can be easily rotated in the event of a breach. 
    By hardcoding passwords, it will be extremely difficult to determine when or 
    if, a key is compromised.

    The recommendation on which KMS to use depends on the environment the application
    is running in:

    - For Google Cloud Platform consider [Cloud Key Management]
      (https://cloud.google.com/kms/docs)
    - For Amazon Web Services consider [AWS Key Management]
      (https://aws.amazon.com/kms/)
    - For on premise or other alternatives to cloud providers, consider 
      [Hashicorp's Vault](https://www.vaultproject.io/)
    - For other cloud providers, please see their documentation
  severity: "ERROR"
  metadata:
    shortDescription: "Use of hard-coded cryptographic key"
    category: "security"
    cwe: "CWE-321"
    owasp:
    - "A2:2017-Broken Authentication"
    - "A07:2021-Identification and Authentication Failures"
    technology:
    - "java"
    security-severity: "CRITICAL"
