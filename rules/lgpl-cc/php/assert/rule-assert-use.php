<?php
// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// source (original): https://github.com/semgrep/semgrep-rules/blob/a3fef245/php/lang/security/assert-use.php
// hash: a3fef245

$tainted = $_GET['userinput'];

// ruleid: assert-use
assert($tainted);

// ok: assert-use
assert('2 > 1');

// todook: assert-use
assert($tainted > 1);

Route::get('bad', function ($name) {
  // ruleid: assert-use
  assert($name);

  // ok: assert-use
  assert('2 > 1');

  // todook: assert-use
  assert($name > 1);
});

?>
