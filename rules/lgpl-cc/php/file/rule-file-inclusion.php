<?php
// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// source (original): https://github.com/semgrep/semgrep-rules/blob/a3fef245/php/lang/security/file-inclusion.php
// hash: a3fef245

$user_input = $_GET["tainted"];

// ruleid: file-inclusion
include($user_input);

// ok: file-inclusion
include('constant.php');

// ruleid: file-inclusion
include_once($user_input);

// ok: file-inclusion
include_once('constant.php');

// ruleid: file-inclusion
require($user_input);

// ok: file-inclusion
require('constant.php');

// ruleid: file-inclusion
require_once($user_input);

// ok: file-inclusion
require_once('constant.php');

// ruleid: file-inclusion
include(__DIR__ . $user_input);

// ok: file-inclusion
include(__DIR__ . 'constant.php');

// ok: file-inclusion
include_safe(__DIR__ . $user_input);

// ok: file-inclusion
require_once(CONFIG_DIR . '/constant.php');

// ok: file-inclusion
require_once( dirname( __FILE__ ) . '/admin.php' );

// ok: file-inclusion
$pth = 'foo/bar.php';
require_once $pth;

?>