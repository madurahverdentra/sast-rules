# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: ruby_csrf_rule-MissingCSRFProtection
  patterns:
  - pattern: |
      class $CONTROLLER < ActionController::Base
        ...
      end
  - pattern-not: |
      class $CONTROLLER < ActionController::Base
        ...
        protect_from_forgery
      end
  message: |
    Detected controller which does not enable cross-site request forgery
    protections using `protect_from_forgery`. When a Rails application does 
    not use `protect_from_forgery` in its controllers, it is vulnerable to 
    CSRF attacks. This vulnerability can allow attackers to submit unauthorized 
    requests on behalf of authenticated users, potentially leading to 
    unauthorized actions being performed. 
    To mitigate this risk, Rails offers the `protect_from_forgery` method, 
    which integrates CSRF token verification in every form submitted to 
    your application, ensuring that requests are legitimate. 
    Add `protect_from_forgery with: :exception` to your controller class. 
    The `with: :exception` option configures Rails to raise an exception 
    if a CSRF token cannot be verified, providing an additional layer of security. 
    This approach ensures that unverified requests do not proceed silently, 
    allowing for appropriate error handling and response measures.
    
    Secure Code Example:
    ```
    class YourController < ApplicationController
      protect_from_forgery with: :exception
      # Your controller actions here
    end
    ```
    For more information on CSRF, see OWASP guide:
    https://owasp.org/www-community/attacks/csrf
  languages:
  - "ruby"
  metadata:
    shortDescription: "Cross-site request forgery (CSRF)"
    category: "security"
    cwe: "CWE-352"
    owasp:
    - "A5:2017-Broken Access Control"
    - "A01:2021-Broken Access Control"
    security-severity: "MEDIUM"
    technology:
    - "ruby"
    references:
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/lang/security/missing-csrf-protection.yaml"
    - "https://owasp.org/Top10/A01_2021-Broken_Access_Control"
  severity: "WARNING"