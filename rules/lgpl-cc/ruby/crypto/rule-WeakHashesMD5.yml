# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: ruby_crypto_rule-WeakHashesMD5
  message: |
    The MD5 hashing algorithm is considered cryptographically weak and 
    vulnerable to collision attacks, where two different inputs generate 
    the same output hash. When used for hashing sensitive data, attackers 
    can exploit this weakness to generate collisions, allowing them to bypass 
    security checks or masquerade malicious data as legitimate. This 
    vulnerability is particularly critical in authentication mechanisms, 
    digital signatures, SSL/TLS certificates, and data integrity checks.

    Remediation:
    To mitigate this vulnerability, replace the MD5 hashing algorithm with 
    stronger cryptographic hash functions, such as SHA-256 or SHA-3. These 
    algorithms offer significantly improved security and are resistant to 
    collision attacks, making them suitable for cryptographic purposes in 
    modern applications.

    Secure Code example:
    ```
    require 'openssl'

    data = "sensitive information"
    # Using SHA-256
    digest = OpenSSL::Digest::SHA256.new
    hash = digest.digest(data)
    hex_hash = digest.hexdigest(data)

    # Using SHA-3 (256 bits)
    digest = OpenSSL::Digest::SHA3.new(256)
    hash2 = digest.digest(data)
    hex_hash2 = digest.hexdigest(data)
    ```
  metadata:
    shortDescription: "Use of weak hash"
    category: "security"
    owasp:
    - "A3:2017-Sensitive Data Exposure"
    - "A02:2021-Cryptographic Failures"
    cwe: "CWE-328"
    security-severity: "MEDIUM"
    references:
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/lang/security/weak-hashes-md5.yaml"
    - "https://www.ibm.com/support/pages/security-bulletin-vulnerability-md5-signature-and-hash-algorithm-affects-sterling-integrator-and-sterling-file-gateway-cve-2015-7575"
    technology:
    - "ruby"
  languages:
  - "ruby"
  severity: "WARNING"
  pattern-either:
  - pattern: Digest::MD5.base64digest $X
  - pattern: Digest::MD5.hexdigest $X
  - pattern: Digest::MD5.digest $X
  - pattern: Digest::MD5.new
  - pattern: OpenSSL::Digest::MD5.base64digest $X
  - pattern: OpenSSL::Digest::MD5.hexdigest $X
  - pattern: OpenSSL::Digest::MD5.digest $X
  - pattern: OpenSSL::Digest::MD5.new