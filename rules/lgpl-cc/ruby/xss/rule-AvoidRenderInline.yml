# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby_xss_rule-AvoidRenderInline"
  pattern: 'render inline: ...'
  message: |
    The application was found calling `render inline: ...` which renders an entire ERB template inline and is potentially dangerous.
    If user supplied input is used, the application can be exploited by malicious 
    actors via server-side template injection (SSTI) or cross-site scripting (XSS) attacks.
    Instead, consider using a partial or another safe rendering method.

    Secure Code Example:
    ```
    # In your controller or view
    user_input = params[:user_input]
    render partial: 'users/user_input', locals: { user_input: user_input }

    # erb file
    <!-- app/views/users/_user_input.html.erb -->
    <!-- Display user input safely -->
    <div>
      <%= sanitize(user_input) %>
    </div>
    ```
  languages: 
  - "ruby"
  severity: "WARNING"
  metadata:
    shortDescription: "Improper neutralization of input during web page generation
      ('Cross-site Scripting')"
    category: "security"
    cwe: "CWE-79"
    owasp:
    - "A7:2017-Cross-Site Scripting (XSS)"
    - "A03:2021-Injection"
    security-severity: "MEDIUM"
    technology:
    - "rails"
    references:
    - "https://brakemanpro.com/2017/09/08/cross-site-scripting-in-rails#inline-renders---even-worse-than-xss"
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/rails/security/audit/xss/avoid-render-inline.yaml"

  
  