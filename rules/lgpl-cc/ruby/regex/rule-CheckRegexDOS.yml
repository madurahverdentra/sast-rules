# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby_regex_rule-CheckRegexDOS"
  mode: taint
  pattern-sources:
  - patterns:
    - pattern-either:
      - pattern: |
          cookies[...]
      - patterns:
        - pattern: |
            cookies. ... .$PROPERTY[...]
        - metavariable-regex:
            metavariable: $PROPERTY
            regex: (?!signed|encrypted)
      - pattern: |
          params[...]
      - pattern: |
          request.env[...]
      - patterns:
        - pattern: $Y
        - pattern-either:
          - pattern-inside: |
              $RECORD.read_attribute($Y)
          - pattern-inside: |
              $RECORD[$Y]
        - metavariable-regex:
            metavariable: $RECORD
            regex: '[A-Z][a-z]+'
  pattern-sinks:
  - patterns:
    - pattern-either:
      - patterns:
        - pattern: $Y
        - pattern-inside: |
            /...#{...}.../
      - patterns:
        - pattern: $Y
        - pattern-inside: |
            Regexp.new(...)
  message: |
    The application was found constructing a regular expression with user-controllable
    input. This may result in excessive resource consumption 
    when applied to certain inputs, or when the user is allowed to control 
    the match target.
    
    To mitigate the issue, avoid allowing users to specify regular expressions 
    processed by the server. If you must support user-controllable input in a 
    regular expression, use an allow-list to restrict the expressions users 
    may supply to limit catastrophic backtracking.

    Secure Code Example:
    ```
    # Define an allow-list of safe, predefined patterns
    ALLOWED_PATTERNS = {
      'digits_only' => /^\d+$/,
      'letters_only' => /^[a-zA-Z]+$/,
      'email_format' => /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/
    }
    user_pattern_key = params[:pattern_key]
    target_data = params[:data]

    if ALLOWED_PATTERNS.key?(user_pattern_key)
      # Fetch the safe pattern from the allow-list
      safe_pattern = ALLOWED_PATTERNS[user_pattern_key]
      
      # Apply the regular expression safely
      if target_data.match(safe_pattern)
        # Do something
      else
        # Do something
      end
    else
      # Handle case where the pattern is not allowed
    end
    ```
  languages:
  - "ruby"
  severity: "WARNING"
  metadata:
    category: "security"
    cwe: "CWE-1333"
    shortDescription: "Inefficient regular expression complexity"
    owasp:
    - "A6:2017-Security Misconfiguration"
    - "A04:2021-Insecure Design"
    security-severity: "MEDIUM"
    technology:
    - "ruby"
    - "rails"
    references:
    - "https://owasp.org/www-community/attacks/Regular_expression_Denial_of_Service_-_ReDoS"
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/rails/security/brakeman/check-regex-dos.yaml"
