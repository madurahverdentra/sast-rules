# yamllint disable
# License: MIT (c) GitLab Inc.
# yamllint enable
---
rules:
- id: "java_inject_rule-SqlInjection"
  languages:
  - "java"
  message: |
    SQL Injection is a critical vulnerability that can lead to data or system compromise. By
    dynamically generating SQL query strings, user input may be able to influence the logic of
    the SQL statement. This could lead to an adversary accessing information they should
    not have access to, or in some circumstances, being able to execute OS functionality or code.

    Replace all dynamically generated SQL queries with parameterized queries. In situations where
    dynamic queries must be created, never use direct user input, but instead use a map or
    dictionary of valid values and resolve them using a user-supplied key.

    For example, some database drivers do not allow parameterized queries for `>` or `<` comparison
    operators. In these cases, do not use a user supplied `>` or `<` value, but rather have the
    user
    supply a `gt` or `lt` value. The alphabetical values are then used to look up the `>` and `<`
    values to be used in the construction of the dynamic query. The same goes for other queries
    where
    column or table names are required but cannot be parameterized.

    Example using `PreparedStatement` queries:
    ```
    // Some userInput
    String userInput = "someUserInput";
    // Your connection string
    String url = "...";
    // Get a connection from the DB via the DriverManager
    Connection conn = DriverManager.getConnection(url);
    // Create a prepared statement
    PreparedStatement st = conn.prepareStatement("SELECT name FROM table where name=?");
    // Set each parameters value by the index (starting from 1)
    st.setString(1, userInput);
    // Execute query and get the result set
    ResultSet rs = st.executeQuery();
    // Iterate over results
    while (rs.next()) {
        // Get result for this row at the provided column number (starting from 1)
        String result = rs.getString(1);
        // ...
    }
    // Close the ResultSet
    rs.close();
    // Close the PreparedStatement
    st.close();
    ```

    For more information on SQL Injection see OWASP:
    https://cheatsheetseries.owasp.org/cheatsheets/SQL_Injection_Prevention_Cheat_Sheet.html
  mode: taint
  options:
    taint_assume_safe_functions: true
  pattern-sources:
  - patterns:
    - pattern: public $FUNC(..., String $SRC, ...) { ... }
    - focus-metavariable: $SRC
  pattern-propagators:
  - pattern: $SB.append($SRC)
    from: $SRC
    to: $SB
  - pattern: $RET = String.format(..., $SRC, ...)
    from: $SRC
    to: $RET
  pattern-sinks:
  - patterns:
    - pattern-either:
      - pattern: "(javax.jdo.PersistenceManager $PM).newQuery($ARG)"
      - pattern: "(javax.jdo.PersistenceManager $PM).newQuery(..., $ARG)"
      - pattern: "(javax.jdo.Query $Q).setFilter($ARG)"
      - pattern: "(javax.jdo.Query $Q).setGrouping($ARG)"
      - pattern: "org.hibernate.criterion.Restrictions.sqlRestriction($ARG, ...)"
      - pattern: "(org.hibernate.Session $S).createQuery((String $ARG), ...)"
      - pattern: "(org.hibernate.Session $S).createSQLQuery($ARG, ...)"
      - pattern: "(java.sql.Statement $S).executeQuery($ARG, ...)"
      - pattern: "(java.sql.Statement $S).execute($ARG, ...)"
      - pattern: "(java.sql.Statement $S).executeUpdate($ARG, ...)"
      - pattern: "(java.sql.Statement $S).executeLargeUpdate($ARG, ...)"
      - pattern: "(java.sql.Statement $S).addBatch($ARG, ...)"
      - pattern: "(java.sql.PreparedStatement $S).executeQuery($ARG, ...)"
      - pattern: "(java.sql.PreparedStatement $S).execute($ARG, ...)"
      - pattern: "(java.sql.PreparedStatement $S).executeUpdate($ARG, ...)"
      - pattern: "(java.sql.PreparedStatement $S).executeLargeUpdate($ARG, ...)"
      - pattern: "(java.sql.PreparedStatement $S).addBatch($ARG, ...)"
      - pattern: "(java.sql.Connection $S).prepareCall($ARG, ...)"
      - pattern: "(java.sql.Connection $S).prepareStatement($ARG, ...)"
      - pattern: "(java.sql.Connection $S).nativeSQL($ARG, ...)"
      - pattern: "new org.springframework.jdbc.core.PreparedStatementCreatorFactory($ARG,
          ...)"
      - pattern: "(org.springframework.jdbc.core.PreparedStatementCreatorFactory $F).newPreparedStatementCreator($ARG,
          ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcOperations $O).batchUpdate($ARG,
          ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcOperations $O).execute($ARG,
          ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcOperations $O).query($ARG, ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcOperations $O).queryForList($ARG,
          ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcOperations $O).queryForMap($ARG,
          ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcOperations $O).queryForObject($ARG,
          ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcOperations $O).queryForObject($ARG,
          ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcOperations $O).queryForRowSet($ARG,
          ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcOperations $O).queryForInt($ARG,
          ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcOperations $O).queryForLong($ARG,
          ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcOperations $O).update($ARG, ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcTemplate $O).batchUpdate($ARG,
          ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcTemplate $O).execute($ARG, ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcTemplate $O).query($ARG, ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcTemplate $O).queryForList($ARG,
          ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcTemplate $O).queryForMap($ARG,
          ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcTemplate $O).queryForObject($ARG,
          ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcTemplate $O).queryForRowSet($ARG,
          ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcTemplate $O).queryForInt($ARG,
          ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcTemplate $O).queryForLong($ARG,
          ...)"
      - pattern: "(org.springframework.jdbc.core.JdbcTemplate $O).update($ARG, ...)"
      - pattern: "(io.vertx.sqlclient.SqlClient $O).query($ARG, ...)"
      - pattern: "(io.vertx.sqlclient.SqlClient $O).preparedQuery($ARG, ...)"
      - pattern: "(io.vertx.sqlclient.SqlConnection $O).prepare($ARG, ...)"
      - pattern: "(org.apache.turbine.om.peer.BasePeer $O).executeQuery($ARG, ...)"
      - pattern: "(org.apache.torque.util.BasePeer $O).executeQuery($ARG, ...)"
      - pattern: "(javax.persistence.EntityManager $O).createQuery($ARG, ...)"
      - pattern: "(javax.persistence.EntityManager $O).createNativeQuery($ARG, ...)"
      - pattern: "(org.jdbi.v3.core.Handle $H).createQuery($ARG, ...)"
      - pattern: "(org.jdbi.v3.core.Handle $H).createScript($ARG, ...)"
      - pattern: "(org.jdbi.v3.core.Handle $H).createUpdate($ARG, ...)"
      - pattern: "(org.jdbi.v3.core.Handle $H).execute($ARG, ...)"
      - pattern: "(org.jdbi.v3.core.Handle $H).prepareBatch($ARG, ...)"
      - pattern: "(org.jdbi.v3.core.Handle $H).select($ARG, ...)"
      - pattern: "new org.jdbi.v3.core.statement.Script($H, $ARG)"
      - pattern: "new org.jdbi.v3.core.statement.Update($H, $ARG)"
      - pattern: "new org.jdbi.v3.core.statement.PreparedBatch($H, $ARG)"
    - focus-metavariable: $ARG
  metadata:
    shortDescription: "Improper Neutralization of Special Elements used in an SQL
      Command ('SQL Injection')"
    category: "security"
    cwe: "CWE-89"
    technology:
    - "java"
    owasp:
    - "A1:2017-Injection"
    - "A03:2021-Injection"
    security-severity: "CRITICAL"
  severity: "ERROR"
